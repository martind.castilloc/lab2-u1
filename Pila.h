#ifndef PILA_H
#define PILA_H

class Pila {

    //Atributos
    private:

        int max;
        int tope;
        int* pila;
        bool band;
        int contador=0;

    public:

    // Constructor de la clase
        Pila(int max);
        
    //Metodos
        //Seters
        void pila_vacia(int tope, bool band);
        void pila_llena();
        void push(int dato);
        void pop();
        void mostrar_pila(int max);
        
};
#endif