prefix=/usr/local
CC = g++
CFLAGS = -g -Wall 
SRC = Main_lab2.cpp Pila.cpp 
OBJ = Main_lab2.o Pila.o
APP = pila

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 
clean:
	$(RM) $(OBJ) $(APP)
install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin
uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)