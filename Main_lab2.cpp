#include <iostream>

#include "Pila.h" 

using namespace std;

void menu(){

    int opc,max,dato;
    int i=0;

    //Se pide el tamaño de la pila, para poder crearla.
    cout << "Ingrese el tamaño de la pila: ";
    cin >> max;
    Pila p = Pila(max);

    do{
        cout << "\n";
        cout << "\t     ---------------------------- " << endl;
		cout << "\t     ***** MENÚ DE OPCIONES ***** " << endl;
		cout << "\t     ---------------------------- " << endl;
		cout << "\n  [1]  > Agregar/push\t" << endl;
		cout << "  [2]  > Remover/pop\t" << endl;
        cout << "  [3]  > Ver Pila\t" << endl;
        cout << "  [4]  > Salir\t" << endl;

        cout << "\nOpción: ";
        cin >> opc;
		cin.ignore();

        if (opc == 1){
            cout << "\n";
            for (int i=0;i<max;i++){ 
                cout << "Ingrese valor: ";              
			    cin >> dato;
			    p.push(dato);//Se guardan los numeros ingresados por el usuario.
		    }
        }

        else if(opc == 2){
            cout << "\n";
            p.pop();
        }

        else if(opc == 3){
            cout << "\n";
            p.mostrar_pila(max);//Se muestran los numeros guardados en la pila.
        }

        else if(opc == 4){
            cout << "\n\t¡Se ha cerrado el programa correctamente!\n" << endl;
			break;
        }

        else{
            cout << "\t--------------------------------------" << endl;
			cout << "\tOpción Invalida. " << endl;
			cout << "\t--------------------------------------\n" << endl;
        }
    }while(i<1);
}

//Se llama a la función menú dentro de la funcion main.
int main(){

    menu();

    return 0;
}