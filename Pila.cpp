#include <iostream>
using namespace std;

#include "Pila.h"


//Constructor con un parametro.
Pila::Pila(int max){
    this->max = max;
    this->pila = new int[max];
	this->band = false;
	this->tope = 0;
    this->contador = 0;
}

void Pila::pila_vacia(int tope, bool band){

    if (tope == 0){
        this->band = true; //Pila vacia.
    }

    else{
        this->band = false;//La pila no esta vacia.
    }
}

void Pila::pila_llena(){

    if (tope == max){
        this->band = true;
    }

    else{
        this->band = false;
    }
}

// Método para llenar la pila
void Pila::push(int dato){

	if(band){
		cout<<"Desbordamiento, pila llena"<<endl;
	}

	else{
		this->tope++;
		this->pila[tope] = dato;
	}
}

//Metodo para eliminar un elemento de la pila
void Pila::pop(){
    if (band){
        cout << "Desbordamiento, pila llena " << endl;
	}
	
	else{

		int num_removido = pila[tope];
        cout << "Valor removido de la pila: " << num_removido << endl;
        pila[tope] = -1;
        tope -= 1;
	}
}

//metodo para mostrar el contenido de la pila
void Pila::mostrar_pila(int max){

    
    for (int i=max;i>-1;i--){

        if (pila[i] > 0){
            cout << "|" << pila[i] << "|" << endl;
        }

        else {
            contador++;
        } 
    }
    
    if (contador > max){
        cout << "No hay elementos apilados" << endl;
    }
    contador = 0;
}





